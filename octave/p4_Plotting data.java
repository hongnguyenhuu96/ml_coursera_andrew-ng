t = [0:0.01:0.98]
y1 = sin(2*pi*4*t);
plot(t,y1); // t horizontal, y1 vertical
y2 = cos(2*pi*4*t);
plot(t,y2);


plot(t,y1);
hold on;
plot(t, y2,'r');
xlabel('time')
ylabel('value')
lengend('sin', 'cos')
title('my plot')
cd Desktop; print -dpng 'myPlot.png'
close


figure(1);
plot(t,y1);
figure(2);
plot(t, y2);

subplot(1,2,1); // devides plot a 1x2 grid, access first element
plot(t, y1);
subplot(1,2,2);
plot(t, y2);
axis([0.5 1 -1 1]) //  set the right figure: x:0.5->1 y:-1->1
help axis
clf; // clear figure



A = magic(5)
imagesc(A); // different color corespond to the value of the matrix
imagesc(A); colorbar, colormap gray;
imagesc(magic(15)), colorbar, colormap gray;

a = 1, b = 2, c = 3
a = 1; b = 2; c = 3;
