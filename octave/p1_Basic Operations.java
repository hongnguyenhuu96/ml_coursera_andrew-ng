5 + 6
3 - 2
5 * 8
1/2
2^6
1==2
1 ~= 2 %false
1 && 0
1 ||0
xor (1,0)

PS('>> ');
PS1('>> ');

a = 3
a = 3; // % dont print the result

b = 'hi';
c = (3 >= 1);

a = pi;
a
disp(a);


disp(sprintf('2 decimals: %0.2f', a))
disp(sprintf('2 decimals: %0.6f', a))

format long
format short

A = [1 2; 3 4; 5 6]
A = [1 2;
3 4;
5 6]

v = [1 2 3]
v = [1;2;3]
v = 1:0.1:2 //% 1 1.1 1.2 ... 2
v = 1:6 //  % 1 2 3 4 5 6


ones(2, 3)
C = 2 *ones(2,3)
C = [2 2 2; 2 2 2]

w = ones(1, 3)
w = zeros(1, 3)

rand(3,3) // 0 <= x =< 1

w = randn(1, 3) // gaussian random // mean zero, standard deviation equal to 1

w = -6 + sqrt(10)*(randn(1, 10000))
hist(w)  // histogram
hist(w,50)


eye(4) // identity matrix row = collum = 4 a[i][i] = 1

help eye
help rand
help help
