
A = [1 2; 3 4; 5 6]
size(A) // return a matrix [row collum] of A
size(A, 1) // return the first dimension of A = 3 in this ex
size (A, 2) // =2
v = [1 2 3 4]
length(v) // return the size of the longest dimension = 4
// v is a 1 * 4 matrix, 4 > 1 so the function return 4
length(A) // = 3  because A(3*2)


pwd // sor the current path in octave
cd Desktop
pwd
ls // list the directory on Desktop


load featuresX.dat // feature in housing : area numOfRoom // equivalent to load('featuresX.dat')
load priceY.dat // price of the house // = load ('priceY.dat')

who // the variables that Octave has in memory currently
featuresX // display featuresX
size(featuresX)
size(priceY)

whos // giving the variables and the details (name, size, bytes, class)
clear featuresX // clear featuresX from the memory, featuresX will disappear

v = priceY(1:10) // the first 10 elements of vector priceY

save hello.mat v // save the variable v in to the file hello.mat
clear // clear anything
load hello.mat

save hello.txt v
save hello.txt -ascii // % save as text

A = [1 2 ; 3 4; 5 6]
A(3,2) // return A[3][2]
A(2,:) // return row 2
A(:,2) // collum 2
A([1 3], :) // return row 1 and 3

A(:, 2) = [10; 11; 12] // replace collum 2 by [10;11;12]
A = [A,[100; 101; 102]] // append another collum to right
A(:) // put all elements of A into a single vector


A = [1 2; 3 4; 5 6]
B = [11 12; 13 14; 15 16]
C = [A B]
C = [A; B]
