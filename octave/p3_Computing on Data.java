A = [1 2; 3 4; 5 6]
B = [11 12; 13 14; 15 16]
C = [1 1; 2 2]

A*C
A .* B // [11 24; 39 56; 75 96]
A .^ 2 // [1 4; 9 16; 25 36]
v = [1; 2; 3]

1 ./ v // [1; 0.5; 1/3]
1 ./ A
exp(v)
abs(v)
abs([-1;2;3]) // [1; 2;3 ]
-v // [-1; -2; -3]
v + ones(length(v), 1) // [2; 3; 4]
equivalent to:  v + 1

// A' // A transpose
// (A')' = A

a = [1 15 2 0.5]
val = max(a) // val = 15
[val, ind] = max(a) // val = 15, ind = 2


max(A) // 5 6 // return max element at each collum
a < 3 // [1<3 15>3 2<3 0.5<3] => [1 0 1 1]
find(a < 3) // [1 3 4] return the position (in vector a) at which the element is less than 3

A = magic(3) // magic(3): matrix 3*3 each row, each collumn and the diagonals  all add up to the same thing
[r, c] = find(A >= 7) // A = [8 1 6; 3 5 7; 4 9 2]
// return r = [1;3;2] c = [1;2;3] element at (1,1) (3,2) (2,3) >= 7

sum(a) // adds up all elements of a
prod(a) // product
floor(a) // rounds down these elements of a 0.5 -> 0
ceil(a) // 0.5 -> 1

max([1 2; 3 4], [-1 3; 5 -2])
// A, B same size
// [1>-1 2<3; 3<5 4>2] => return [1 3; 5 4]

A = [8 1 6; 3 5 7; 4 9 2]
max(A,[],1) // return max elements per collumn [8 9 7]
max(A,[],2) // return max elements per row [8;7;9]

max(max(A)) // return the max element in A
max(A(:)) // return the max element in A // A(:) turn A in to vector


A = magic(9)
sum(A, 1) // sum per collum = sum(A) [369 .... 369]
sum(A, 2) //  [369; 369; ... ; 369]

A = [1 0;
     0 1]
flipud(A) // ans = [0 1; 1 0]

pinv(A) // pseudo - inverse of A
